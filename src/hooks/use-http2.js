import { useCallback, useState } from "react";

const useHttp = (callback) => {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isComplete, setIsComplete] = useState(false);

    const sendRequest = useCallback(
        async (data) => {
            setIsLoading(true);
            setError(null);
            try {
                const responseData = await callback(data);
                setData(responseData);
            } catch (error) {
                setError(error || 'Request failed!');
            }
            setIsLoading(false);
            setIsComplete(true);
        }, [callback]);

    return { data, error, isLoading, isComplete, sendRequest };
}

export default useHttp;