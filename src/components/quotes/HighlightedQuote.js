import { Fragment } from 'react';

import classes from './HighlightedQuote.module.css';

const HighlightedQuote = (props) => {
  return (
    <Fragment>
      <figure className={classes.quote}>
        <blockquote>
          <p>{props.text}</p>
        </blockquote>
        <figcaption>{props.author}</figcaption>
      </figure>
    </Fragment>
  );
};

export default HighlightedQuote;
