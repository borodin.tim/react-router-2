import { useRef, useEffect } from 'react';

import classes from './NewCommentForm.module.css';
import useHttp from '../../hooks/use-http2';
import { addComment } from '../../lib/api2';
import LoadingSpinner from '../UI/LoadingSpinner';

const NewCommentForm = (props) => {
  const commentTextRef = useRef();
  const { _, error, isLoading, isComplete, sendRequest } = useHttp(addComment);

  const { onAddCommentFinish } = props;

  useEffect(() => {
    if (isComplete && !error) {
      onAddCommentFinish();
    }
  }, [isComplete, error, onAddCommentFinish]);

  const submitFormHandler = (event) => {
    event.preventDefault();

    const text = commentTextRef.current.value;

    if (!text || text.length === 0) {
      return;
    }

    sendRequest({
      text,
      quoteId: props.quoteId
    });
  };

  if (error) {
    return (
      <p className="centered">{error}</p>
    );
  }

  if (isLoading) {
    return (
      <div className="centered">
        <LoadingSpinner />
      </div>
    );
  }

  return (
    <form className={classes.form} onSubmit={submitFormHandler}>
      <div className={classes.control}>
        <label htmlFor='comment'>Your Comment</label>
        <textarea
          id='comment'
          rows='3'
          ref={commentTextRef}
        />
      </div>
      <div className={classes.actions}>
        <button className='btn'>Add Comment</button>
      </div>
    </form>
  );
};

export default NewCommentForm;
