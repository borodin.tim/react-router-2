import { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router';

import classes from './Comments.module.css';
import CommentsList from './CommentsList';
import NewCommentForm from './NewCommentForm';
import useHttp from '../../hooks/use-http2';
import { getAllCommentsUnderPost } from '../../lib/api2';
import LoadingSpinner from '../UI/LoadingSpinner';

const Comments = () => {
  const [isAddingComment, setIsAddingComment] = useState(false);
  const { data: loadedComments, error, isLoading, isComplete, sendRequest } = useHttp(getAllCommentsUnderPost);
  const params = useParams();
  const quoteId = params.quoteId;

  useEffect(() => {
    sendRequest(quoteId);
  }, [sendRequest, quoteId, isAddingComment]);

  const startAddCommentHandler = () => {
    setIsAddingComment(true);
  };

  const handleAddCommentFinish = useCallback(() => {
    setIsAddingComment(false)
  }, []);

  return (
    <section className={classes.comments}>
      <h2>User Comments</h2>
      {!isAddingComment && (
        <button type="button" className='btn' onClick={startAddCommentHandler}>
          Add a Comment
        </button>
      )}
      {isAddingComment && <NewCommentForm
        onAddCommentFinish={handleAddCommentFinish}
        quoteId={quoteId}
      />}
      {isComplete && !error && <CommentsList comments={loadedComments} />}
      {isLoading && <div className="centered"><LoadingSpinner /></div>}
      {error && <p className="centered">{error}</p>}
    </section>
  );
};

export default Comments;
