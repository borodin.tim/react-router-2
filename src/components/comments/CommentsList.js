import CommentItem from './CommentItem';
import classes from './CommentsList.module.css';

const CommentsList = (props) => {
  return (
    <ul className={classes.comments}>
      {props.comments && props.comments.length > 0 && props.comments.map((comment) => (
        <CommentItem key={comment.id} text={comment.text} />
      ))}
      {(!props.comments || props.comments.length === 0) && <p className="centered">No comments added yet</p>}
    </ul>
  );
};

export default CommentsList;
