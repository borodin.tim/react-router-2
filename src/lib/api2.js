const FIREBASE_URL = 'https://react-redux-app-344e0-default-rtdb.europe-west1.firebasedatabase.app/';

export async function getAllQuotes() {
    const response = await fetch(`${FIREBASE_URL}/quotes.json`);
    const data = await response.json();

    if (!response.ok) {
        throw new Error(data.message || 'Error sending request')
    }

    const transformedQuotes = [];
    for (const key in data) {
        transformedQuotes.push({
            id: key,
            ...data[key]
        });
    }

    return transformedQuotes;
};

export const getSingleQuote = async (quoteId) => {
    const response = await fetch(`${FIREBASE_URL}/quotes/${quoteId}.json`);
    const data = await response.json();

    if (!response.ok) {
        throw new Error(data.message || 'Error sending request')
    }

    const transformedQuote =
    {
        id: data.key,
        ...data
    };

    return transformedQuote;
}

export const addQuote = async (newQuote) => {
    const response = await fetch(`${FIREBASE_URL}/quotes.json`, {
        method: 'POST',
        body: JSON.stringify(newQuote),
        headers: { 'Content-Type': 'application/json' }
    });
    const data = await response.json();

    if (!response.ok) {
        throw new Error(data.message || 'Error sending request');
    }
}

export const getAllCommentsUnderPost = async (postId) => {
    const response = await fetch(`${FIREBASE_URL}/comments/${postId}.json`);
    const data = await response.json();

    if (!response.ok) {
        throw new Error(data.message || 'Error sending request')
    }

    const transformedQuotes = [];
    for (const key in data) {
        transformedQuotes.push({
            id: key,
            text: data[key]
        });
    }

    return transformedQuotes;
};

export const addComment = async ({ text, quoteId }) => {
    const response = await fetch(`${FIREBASE_URL}/comments/${quoteId}.json`, {
        method: 'POST',
        body: JSON.stringify(text),
        headers: { 'Content-Type': 'application/json' }
    });
    const data = await response.json();

    if (!response.ok) {
        throw new Error(data.message || 'Error sending request');
    }
}