import { useEffect } from "react";

import QuoteList from "../components/quotes/QuoteList";
import useHttp from "../hooks/use-http2";
import { getAllQuotes } from "../lib/api2";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import NoQuotesFound from "../components/quotes/NoQuotesFound";

const AllQuotes = () => {
    const { data: loadedQuotes, error, isLoading, isComplete, sendRequest } = useHttp(getAllQuotes);

    useEffect(() => {
        sendRequest();
    }, [sendRequest]);

    if (isLoading) {
        return (
            <div className="centered">
                <LoadingSpinner />
            </div>
        );
    }

    if (error) {
        return (<p className="centered focus">{error}</p>);
    }

    if (isComplete && (!loadedQuotes || loadedQuotes.length === 0)) {
        return (<NoQuotesFound />);
    }

    return (
        <QuoteList isLoading={isLoading} quotes={loadedQuotes} />
    )
}

export default AllQuotes;