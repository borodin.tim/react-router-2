import { Fragment } from "react";
import { useParams, Route, Link, useRouteMatch } from "react-router-dom";

import HighlightedQuote from "../components/quotes/HighlightedQuote";
import Comments from "../components/comments/Comments";
import useHttp from "../hooks/use-http2";
import { getSingleQuote } from "../lib/api2";
import { useEffect } from "react/cjs/react.development";
import LoadingSpinner from "../components/UI/LoadingSpinner";

const Quote = () => {
    const { quoteId } = useParams();
    const match = useRouteMatch();
    const { data: loadedQuote, error, isLoading, isComplete, sendRequest } = useHttp(getSingleQuote);

    useEffect(() => {
        sendRequest(quoteId);
    }, [quoteId, sendRequest]);

    if (isLoading) {
        return (
            <div className="centered">
                <LoadingSpinner />
            </div>
        );
    }

    if (error) {
        return (<p className="centered focus">{error}</p>);
    }

    if (!loadedQuote || !loadedQuote.text) {
        return (<p className="centered">No quote found</p>);
    }

    return (
        <Fragment>
            <HighlightedQuote text={loadedQuote.text} author={loadedQuote.author} />
            <Route path={match.path} exact>
                <div className="centered">
                    <Link className="btn--flat" to={`${match.url}/comments`}>
                        Load Comments
                    </Link>
                </div>
            </Route>
            <Route path={`${match.path}/comments`}>
                <Comments />
            </Route>
        </Fragment>
    );
}

export default Quote;