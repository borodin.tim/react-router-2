import { useHistory } from 'react-router-dom';
import { Fragment, useEffect } from 'react';

import QuoteForm from "../components/quotes/QuoteForm"
import { addQuote } from '../lib/api2';
import useHttp from '../hooks/use-http2';

const NewQuote = () => {
    const history = useHistory();
    const { _, error, isLoading, isComplete, sendRequest } = useHttp(addQuote);

    useEffect(() => {
        if (isComplete) {
            history.push('/quotes');
        }
    }, [isComplete, history]);

    const handleAddQuote = (quoteData) => {
        sendRequest(quoteData);
    }

    return (
        <Fragment>
            {error && <p>Error!</p>}
            <QuoteForm isLoading={isLoading} onAddQuote={handleAddQuote} />
        </Fragment>
    );
}
export default NewQuote;