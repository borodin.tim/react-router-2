# react-router-dom project

### Hooks from react-router-dom
- BrowserRouter
- Link
- NavLink
- Route
- Switch
- Prompt

### Components from react-router-dom:
- useParams
- useLocation
- useHistory
- useRouteMatch

### Optimizations:
Lazy loading